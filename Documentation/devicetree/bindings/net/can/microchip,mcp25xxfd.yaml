# SPDX-License-Identifier: (GPL-2.0 OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/net/can/microchip,mcp25xxfd.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Microchip MCP25XXFD stand-alone CAN controller binding

maintainers:
  -  Martin Sperl <kernel@martin.sperl.org>
  -  Manivannan Sadhasivam <manivannan.sadhasivam@linaro.org>

properties:
  compatible:
    const: microchip,mcp2517fd

  reg:
    maxItems: 1

  clocks:
    maxItems: 1

  interrupts:
    maxItems: 1

  gpio-controller: true

  "#gpio-cells":
    const: 2

  vdd-supply:
    description: Regulator that powers the CAN controller

  xceiver-supply:
    description: Regulator that powers the CAN transceiver

  microchip,clock-out-div:
    description: Clock output pin divider
    allOf:
      - $ref: /schemas/types.yaml#/definitions/uint32
    enum: [0, 1, 2, 4, 10]
    default: 10

  microchip,clock-div2:
    description: Divide the internal clock by 2
    type: boolean

  microchip,gpio-open-drain:
    description: Enable open-drain for all pins
    type: boolean

required:
  - compatible
  - reg
  - clocks
  - interrupts
  - gpio-controller
  - vdd-supply
  - xceiver-supply

additionalProperties: false

examples:
  - |
    spi {
           #address-cells = <1>;
           #size-cells = <0>;

           can0: can@1 {
                   compatible = "microchip,mcp2517fd";
                   reg = <1>;
                   clocks = <&clk24m>;
                   interrupt-parent = <&gpio4>;
                   interrupts = <13 0x8>;
                   vdd-supply = <&reg5v0>;
                   xceiver-supply = <&reg5v0>;
                   gpio-controller;
                   #gpio-cells = <2>;
           };
    };

...
